﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadEnemy : MonoBehaviour
{
    //Variables Called Upon
    private Rigidbody rb;
    public float force;
    bool applyForce = true;

    //On Awake this will add a little force for when the enemy is dead.
    public void Awake()
    {
        rb = GetComponent<Rigidbody>();
        transform.rotation = Random.rotation;
        force = Random.Range(800f, 1600f);
        rb.AddForce(transform.up * force);
        Destroy(gameObject, 3);
    }
}
