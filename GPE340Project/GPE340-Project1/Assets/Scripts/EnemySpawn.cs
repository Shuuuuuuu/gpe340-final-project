﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    //Variables
    public GameObject enemyPrefab;
  
    //This will Spawn our enemies
    private void Start()
    {
        Invoke("SpawnEnemy", 10);
    }

    //This will invoke our adding of enemies and where
    public void SpawnEnemy()
    {
        GameObject enemy = (GameObject)Instantiate(enemyPrefab, this.transform.position, Quaternion.identity);
        Invoke("SpawnEnemy", Random.Range(1, 10));
    }

    //Gizmo that will let you know where the Spawn for the enemy is
    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 1);
    }
}
