﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    //Variables
    public GameObject Bullet;

    public float force = 2000f;
    public AudioSource gunAudio;

    //When we press space we will instantiate bullets and fire our enemy.
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            gunAudio.Play();
            GameObject BulletHolder;
            BulletHolder = Instantiate(Bullet, transform.position, transform.rotation) as GameObject;
            BulletHolder.transform.Rotate(Vector3.left * 90);
            Rigidbody Temporary_RigidBody;
            Temporary_RigidBody = BulletHolder.GetComponent<Rigidbody>();
            Temporary_RigidBody.AddForce(transform.forward * force);
            Destroy(BulletHolder, 5.0f);
        }
    }
}
