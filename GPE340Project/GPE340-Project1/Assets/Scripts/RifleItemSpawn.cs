﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleItemSpawn : MonoBehaviour
{
    //Variables that makes the item float
    private float degreesPerSecond = 15f;
    private float amplitude = 0.5f;
    private float frequency = 1;

    //Position Variables
    Vector3 posOffSet = new Vector3();
    Vector3 tempPos = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        posOffSet = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Spin Around
        transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        //Float
        tempPos = posOffSet;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        transform.position = tempPos;
    }
}
