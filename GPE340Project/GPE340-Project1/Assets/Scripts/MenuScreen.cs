﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScreen : MonoBehaviour
{
    public Transform canvas;
   
    //When you push PVE Load the PVE
    public void StartGame()
    {
        SceneManager.LoadScene("MainGame");
        Time.timeScale = 1;
    }

    public void HowToPlay()
    {
        SceneManager.LoadScene("HowToPlay");
    }

    //Go Back to Menu
    public void Back()
    {
        SceneManager.LoadScene("StartMenu");
    }

    public void OnResume()
    {
        PauseMenu.gameIsPaused = false;
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
        }
        else
        {
            canvas.gameObject.SetActive(false);
        }
        Time.timeScale = 1;
    }

    //Push Quit Exits the game
    public void Quit()
    {
        Application.Quit();
        Debug.Log("Application Quit");
    }
}
