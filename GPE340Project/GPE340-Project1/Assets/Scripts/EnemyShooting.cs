﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    //Variables Used
    public GameObject bullet;
    private float timerShots;
    public float rateShots = 0.25f;
    public float fireRadius = 25f;
    public float force = 2000f;
    public AudioSource gunAudio;


    public void Start()
    {
        gunAudio = GetComponent<AudioSource>();
    }
    //Get the player position and if in distance AI will fire
    public void Update()
    {
        float distance = Vector3.Distance(Pawn.playerPos, transform.position);
        if (distance <= fireRadius)
        { 
            AIFireBullet();
        }
    }

    //This will let know the AI when to fire and to instantiate the bullets to fire.
    public void AIFireBullet()
    {
        RaycastHit hitPlayer;
        Ray playerPos = new Ray(transform.position, transform.forward);
        if(Physics.SphereCast(playerPos,0.25f,out hitPlayer, fireRadius))
        {

            if(timerShots <= 0 && hitPlayer.transform.tag == "Player")
            {
                gunAudio.Play();
                GameObject BulletHolder;
                BulletHolder = Instantiate(bullet, transform.position, transform.rotation) as GameObject;
                BulletHolder.transform.Rotate(Vector3.left * 90);
                Rigidbody tempRigidBody;
                tempRigidBody = BulletHolder.GetComponent<Rigidbody>();
                tempRigidBody.AddForce(transform.forward * force);
                Destroy(BulletHolder, 2.0f);
                timerShots = rateShots;
            }
            else
            {
                timerShots -= Time.deltaTime;
            }
        }
    }
}
