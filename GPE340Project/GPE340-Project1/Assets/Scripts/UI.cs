﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    //If true the rotation for HP Ui will work
    public bool useRelativeRotation = true;
    //Quaternion use to rotate in a circle
    private Quaternion relativeRotation;

    //On Start uses relativeRotation to fill the Hp bar.
    private void Start()
    {
        relativeRotation = transform.parent.localRotation;
    }

    //This will update our HP UI when we take damage or gain health
    private void Update()
    {
        if(useRelativeRotation)
        {
            transform.rotation = relativeRotation;
        }
    }
}
