﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pawn : MonoBehaviour
{
    //Variables for the Health
    [Header("Player Stats")]
    public float maxHealth = 100f;
    public Slider slider;
    public Image fillImage;
    public Color fullHpColor = Color.cyan;
    public Color zeroHpColor = Color.red;

    //Variables called as Animator
    [Header("Calls in the Animator")]
    [SerializeField]
    private Animator animator;
    [SerializeField, Tooltip("The speed of where the character will be facing whe moving the mouse.")]
    private float speed = 5f;
    public float curHealth;
    public static Vector3 playerPos;
   
   
    [Header("UI")]
    public int score;
    public int lives;
    public Text scoreText;
    public Text livesText;

    // Start is called before the first frame update
    void Start()
    {
        //Get Component to use it in the Movement Script
        animator = GetComponent<Animator>();
        StartCoroutine(TrackPlayer());
        score = 0;
        scoreText.text = "Score: " + score;
        livesText.text = "Lives: " + lives;
    }

    // Update is called once per frame
    void Update()
    {
        // Character moves along the X/Z axes when given the input
        Vector3 input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        input = Vector3.ClampMagnitude(input, 1f);
        input = transform.InverseTransformDirection(input);
        OnPick();
        SetHpUI();
        OnDeath();
    }

    /// <summary>
    /// Gets the direction from x and y values and multiplies by the speed to know how fast your chracter will look for the mouse.
    /// </summary>
    /// <param name="direction"></param>
    public void Move(Vector3 direction)
    {
        //Players speed determined by how far the stick is times speed
        animator.SetFloat("Horizontal", direction.x * speed);
        animator.SetFloat("Vertical", direction.y * speed);
    }

    //This tells us to enable our Health UI and lets it know that we are not dead.
    public void OnEnable()
    {
        curHealth = maxHealth;
        SetHpUI();
    }

    //When we take damage our health is decreased and when we reach 0 starts On Death function.
    public void TakeDamage (float damage)
    {
        curHealth -= damage;
        SetHpUI();
    }

    //Sets up our health UI and moves the slider acordingly
    public void SetHpUI()
    {
        slider.value = curHealth;
        fillImage.color = Color.Lerp(zeroHpColor, fullHpColor, curHealth / maxHealth);
    }

    //This will call the death function and destroy our game object
    public void OnDeath()
    {
        if (curHealth <= 0f)
        {
            lives--;
            livesText.text = "Lives: " + lives;
            SceneManager.LoadScene("GameOver");
        }

    }

    //Little Animation to use to pick up items
    private void OnPick()
    {
        if(Input.GetKey(KeyCode.E))
        {
            animator.SetBool("isPickup", true);
        }

        else
        {
            animator.SetBool("isPickup", false);
        }
    }

    //When collide with enemy takes damage
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            curHealth -= 10;
        }
        if(other.CompareTag("Bullet"))
        {
            curHealth -= 10;
        }

    }

    //When collide with the health packet gains health.
    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Healthitem")
        {
            SetHpUI();
        }
    }

    //This tracks the players position at all times for the AI to work.
    IEnumerator TrackPlayer()
    {
        while(true)
        {
            playerPos = gameObject.transform.position;
            yield return null;
        }
    }

    public void ScorePoints(int pointsToAdd)
    {
        score += pointsToAdd;
        scoreText.text = "Score: " + score;
    }
}
