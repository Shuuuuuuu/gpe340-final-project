﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour
{
    //Inputs
    public GameObject health;
    public Transform[] spawnPoints;
    private float spawnTime = 5f;
    
    //On Start it will spawn the clones with spawn time variable
    private void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    //This will determine where and what object will spawned and where.
    void Spawn()
    {
        int spawnIndex = Random.Range(0, spawnPoints.Length);
        Instantiate(health, spawnPoints[spawnIndex].position, spawnPoints[spawnIndex].rotation);
    }
    
}
