﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnImpact : MonoBehaviour
{
    //Variables
    public GameObject impactEffect;
    public float damage = 25f;

    //When it hits the enemy it takes damage also has the impact effect to work where we are looking.
    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.contacts[0];
        Instantiate(impactEffect, contact.point, Quaternion.LookRotation(contact.normal));
        if(collision.gameObject.tag == "Enemy")
        {
            Enemy target = collision.transform.gameObject.GetComponent<Enemy>();
            target.Damage(damage);
        }
        Destroy(gameObject);
    }
}
