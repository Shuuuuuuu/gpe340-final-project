﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyOnImpact : MonoBehaviour
{
    //Variables
    public GameObject impactEffect;
    public float damage = 25f;

    //When collides it hits the player taking damage
    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint contact = collision.contacts[0];
        Instantiate(impactEffect, contact.point, Quaternion.LookRotation(contact.normal));
        if (collision.gameObject.tag == "Player")
        {
            Pawn curHealth = collision.transform.gameObject.GetComponent<Pawn>();
            curHealth.TakeDamage(damage);
        }
        Destroy(gameObject);
    }
}
